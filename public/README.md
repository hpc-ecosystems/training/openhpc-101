
# OpenHPC 101

(Please note: owing to the deprecation of CentOS 7 and *xCAT* in OpenHPC, the content delivered in this guide is no longer aligned with the current deployment stack that is used in the **HPC Ecosystems** Project as of 2024 onwards - the latest guide at the time of this post is available at https://hpc-ecosystems.gitlab.io/training/openhpc-2.x-guide/)  

With that out of the way, if you wish to continue with this lab, **welcome to the HPC Ecosystems OpenHPC 1.3.x Training Guide!** We are so thrilled to have you along for the journey.

In the sections to follow you will find a comprehensive HOW-TO guide which can be used to set up OpenHPC&nbsp;1.3.x on a virtual cluster. This guide was developed for HPC Ecosystems Community as training material for our sites, to ensure parity across their HPC software environments. 

That being said, you **DO NOT** have to belong to the HPC Ecosystems Community to use this guide. Anybody wishing to learn more about the deployment of the OpenHPC&nbsp;1.3.x software stack is likely to find value in its contents. Let's jump right in!
