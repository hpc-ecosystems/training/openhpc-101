(function () {
    function syntax_select(hook) {
        hook.doneEach(function () {
            const allListItems = document.querySelectorAll("pre");
            let indexCodeItem = 0;

            allListItems.forEach(function (el, index) {
                if (el.firstChild.nodeName === "CODE") {
                    const wrapper = document.createElement('div')
                    const element = allListItems[index];

                    wrapper.setAttribute('id', `syntax-select--${indexCodeItem}`)
                    wrapper.classList.add('syntax-select-container')

                    Object.values(el.children).forEach(function (childElement, childIndex) {
                        if (childElement.innerText.includes(']# ') || childElement.innerText.includes(']$ ')) {
                            token = ""
                            if (childElement.innerText.includes(']# ')) {
                                token = '# '
                            } else {
                                token = '$ '
                            }
                            currHTML = childElement.innerHTML.split("\n")
                            newHTML = ''
                            for (ele = 0; ele < currHTML.length; ele++) {
                                ("ELEMENT content: " + currHTML[ele])
                                newHTML = newHTML + "<span class='disableCopy'>" + currHTML[ele]
                                if (newHTML.includes('<span class="token punctuation">]</span><span class="token comment"># ')) {
                                    newHTML = newHTML.replaceAll('<span class="token punctuation">]</span><span class="token comment"># ', '<span class="token punctuation">]</span># </span>')
                                } else if (newHTML.includes('<span class="token punctuation">]</span>$ ')) {
                                    newHTML = newHTML.replaceAll('<span class="token punctuation">]</span>$ ', '<span class="token punctuation">]</span>$ </span>')
                                } else {
                                    newHTML += "</span>"
                                }
                                newHTML += "\n"
                            }
                            childElement.innerHTML = newHTML

                        }
                    })
                    indexCodeItem++;
                }
            });
        });
        Prism.highlightAll();
    }

    window.$docsify.plugins = [].concat(syntax_select, $docsify.plugins)
}());